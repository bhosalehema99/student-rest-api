package com.student.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.student.model.Student;
import com.student.repository.StudentRepo;

@RestController
@RequestMapping("/api/v1/")
public class StudentController {

	@Autowired
	private StudentRepo studentrepo;
	
	@PostMapping("student")
	public Student createStudent(@RequestBody Student stu) {
		return this.studentrepo.save(stu);	
	}
	
	@GetMapping("student/{id}")
	public Optional<Student> getStudent(@PathVariable("id") Long studid) {
		
	   Optional<Student> local = this.studentrepo.findById(studid);	
	   if(local != null) {
		   System.out.println("Student not exist!!");
	   }

	   return local;
	}
	
	@DeleteMapping("student/{studid}")
	public void deleteStudent(@PathVariable Long studid) {
	     this.studentrepo.deleteById(studid);	    	   
	}
	
	@SuppressWarnings("deprecation")
	@PutMapping("student/{studid}")
	public Student updateStudent(@PathVariable("studid") Long studid ,@RequestBody Student student){
		Student byId = this.studentrepo.getById(studid);

			byId.setStudentclass(student.getStudentclass());
			byId.setStudentname(student.getStudentname());
		return this.studentrepo.save(byId);
	}
}
