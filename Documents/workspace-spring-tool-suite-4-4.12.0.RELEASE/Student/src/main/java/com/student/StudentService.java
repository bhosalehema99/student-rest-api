package com.student;

import com.student.model.Student;

public interface StudentService {
    public Student createStudent(Student student);
}
