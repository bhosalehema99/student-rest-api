package com.student.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;  

@Entity
public class Student {
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", studentname=" + studentname + ", studentclass=" + studentclass + "]";
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStudentname() {
		return studentname;
	}
	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}
	public String getStudentclass() {
		return studentclass;
	}
	public void setStudentclass(String studentclass) {
		this.studentclass = studentclass;
	}
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Student(long id, String studentname, String studentclass) {
		super();
		this.id = id;
		this.studentname = studentname;
		this.studentclass = studentclass;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	 private long id;
	 private String studentname;
	 private String studentclass;
}
